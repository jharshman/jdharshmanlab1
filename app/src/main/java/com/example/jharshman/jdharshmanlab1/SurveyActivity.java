package com.example.jharshman.jdharshmanlab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SurveyActivity extends AppCompatActivity {

    private TextView mTextView;
    private Button mB1;
    private EditText mEditText;
    private String mName;
    private int mAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);

        //initialize submit button
        initButton();

        //check for incoming intent
        if(getIntent() != null) {
            Intent intent = getIntent();
            mName = intent.getStringExtra("form_value");
            mTextView = (TextView)findViewById(R.id.textView2);
            mTextView.setText("Hello " + mName);
        }

    }

    public void initButton() {
        mB1 = (Button)findViewById(R.id.button3);
        mEditText = (EditText)findViewById(R.id.editText2);
        mB1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //get age
                String temp = mEditText.getText().toString();
                if(temp != null) {
                    try {
                        mAge = Integer.parseInt(temp);
                        handleAge(mAge);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        CharSequence text = "Please enter your age";
                        Toast toast = Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }
            }
        });
    }

    public void handleAge(int pAge) {
        Intent result = new Intent();

        //Todo: (DONE) Moved to onActivityResult() in MainActivity
        //int data = (pAge < 40) ? 1 : 0;

        result.putExtra("data", pAge);
        setResult(RESULT_OK, result);
        finish();
    }

}
