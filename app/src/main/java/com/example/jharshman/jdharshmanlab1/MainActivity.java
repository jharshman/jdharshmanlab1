package com.example.jharshman.jdharshmanlab1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends TracerActivity {

    private Button mB1;
    private Button mB2;
    private EditText mEditText;
    private TextView mTextView;
    private String mName;

    static final int SURVEY_RESULT_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        no point in handling such few resources anywhere else
         */
        mEditText = (EditText)findViewById(R.id.editText);
        mTextView = (TextView)findViewById(R.id.textView4);

        Intent intent = getIntent();
        if(intent != null) {
            String action = intent.getAction();
            String type = intent.getType();
            if(Intent.ACTION_SEND.equals(action) && "text/plain".equals(type)) {
                mTextView.setText(intent.getStringExtra(Intent.EXTRA_TEXT));
            }
        }
        initButtons();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.action_about) {
            aboutMe();
        }

        return super.onOptionsItemSelected(item);
    }

    public void initButtons() {

        mB1 = (Button)findViewById(R.id.button);
        mB2 = (Button)findViewById(R.id.button2);

        mB1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(mEditText.getText().length() > 0) {
                    mName = mEditText.getText().toString();
                    Intent intent = new Intent(view.getContext(), SurveyActivity.class);
                    intent.putExtra("form_value", mName);
                    MainActivity.this.startActivityForResult(intent, SURVEY_RESULT_CODE);
                }
                else {
                    CharSequence text = "Please enter your name";
                    Toast toast = Toast.makeText(view.getContext(), text, Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });

        mB2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Uri website = Uri.parse("https://sites.google.com/site/pschimpf99/");
                Intent intent = new Intent(Intent.ACTION_VIEW, website);
                startActivity(intent);
            }
        });

    }

    public void aboutMe() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.attr_dialog_title)
                .setItems(R.array.about_me, new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });

        builder.create();
        builder.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 0 && resultCode == Activity.RESULT_OK) {
            int age = data.getIntExtra("data", 0);
            int result = (age<40)? 1 : 0;
            if(result == 1) mTextView.setText(R.string.trustworthy);
            else            mTextView.setText(R.string.not_trustworthy);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}



