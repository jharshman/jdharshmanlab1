package com.example.jharshman.jdharshmanlab1;

import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Iterator;
import java.util.Set;

/**
 * Created by jharshman on 10/4/15.
 */
public class TracerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null)
            notify("onCreate NO state");
        else {
            notify("onCreate WITH state");
            Set<String> keys = savedInstanceState.keySet();
            Iterator iterator = keys.iterator();
            while(iterator.hasNext())
                notify("key: " + iterator.next());
        }


    }

    private void notify(String msg) {
       String strClass = this.getClass().getName();
        String[] strings = strClass.split("\\. ");
        Notification.Builder notificationBuilder = new Notification.Builder(this);
        notificationBuilder.setContentTitle(msg);
        notificationBuilder.setAutoCancel(true).setSmallIcon(R.mipmap.ic_launcher);
        notificationBuilder.setContentText(strings[strings.length-1]);
        Notification notification = notificationBuilder.build();
        NotificationManager notificationManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify((int)System.currentTimeMillis(), notification);
    }


}
